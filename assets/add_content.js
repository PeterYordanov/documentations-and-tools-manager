function addEntryJS(evt) {
    var categoryName = document.getElementById('categoryName').value;
	var title = document.getElementById('title').value;
	var url = document.getElementById('url').value;
	
	addEntry(categoryName, title, url);
	
	UIkit.notification({message: '<span class=\'uk-background-secondary\' uk-icon=\'icon: check\'></span> Successfully added entry'})
}