#ifndef JSONREADERWRITER_HPP
#define JSONREADERWRITER_HPP

#include <json/json.h>
#include <iostream>
#include "file.hpp"
#include "stringhelpers.hpp"

class JsonReaderWriter {
public:
    JsonReaderWriter(const std::string& path) :
        m_path(path)
    {
        File file(stringToWString(m_path));
        m_data = Json::Value(read(file.readAllText()));
    }

    Json::Value read(const std::string& rawJson) {
        const auto rawJsonLength = static_cast<int>(rawJson.length());
        Json::String err;
        Json::Value root;

        Json::CharReaderBuilder builder;
        const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
        bool success = reader->parse(rawJson.c_str(),
                      rawJson.c_str() + rawJsonLength,
                      &root,
                      &err);
        return root;
    }

    void append(const Json::Value& item) {
        m_data.append(item);
    }

    void removeByTitle(const std::string& key) {
        Json::Value temp;
        for(int i = 0; i < m_data.size(); i++) {
            if(m_data.get(i, &temp)["title"] == key)
                m_data.removeIndex(i, &temp);
        }
    }

    Json::Value getData() {
        return m_data;
    }

    void save() {
        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, m_data);
        std::cout << json_file;

        File file(stringToWString(m_path));
        file.writeAllText(json_file);
    }

private:
    Json::Value m_data;
    std::string m_path;
};

#endif // JSONREADERWRITER_HPP
