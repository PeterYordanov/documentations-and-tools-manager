#ifndef STRINGHELPERS_HPP
#define STRINGHELPERS_HPP

#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <cctype>
#include <algorithm>

inline std::wstring stringToWString(const std::string& s)
{
    std::wstring temp(s.length(), L' ');
    std::copy(s.begin(), s.end(), temp.begin());
    return temp;
}

inline std::string wStringToString(const std::wstring& s)
{
    std::string temp(s.length(), ' ');
    std::copy(s.begin(), s.end(), temp.begin());
    return temp;
}

std::wstring toLower(std::wstring source);
std::wstring toUpper(std::wstring source);
std::wstring trimStart(std::wstring source, const std::wstring& trimChars = L" \t\n\r\v\f");
std::wstring trimEnd(std::wstring source, const std::wstring& trimChars = L" \t\n\r\v\f");
std::wstring trim(std::wstring source, const std::wstring& trimChars = L" \t\n\r\v\f");
std::wstring replace(std::wstring source, const std::wstring& find, const std::wstring& replace);
bool         startsWith(const std::wstring& source, const std::wstring& value);
bool         endsWith(const std::wstring& source, const std::wstring& value);
std::vector<std::wstring> split(const std::wstring& source, wchar_t delimiter);
bool         isEmptyOrWhiteSpace(const std::wstring& source);

template<typename T>
std::wstring toString(const T& subject)
{
    std::wostringstream ss;
    ss << subject;
    return ss.str();
}

template<typename T>
T fromString(const std::wstring& subject)
{
    std::wistringstream ss(subject);
    T target;
    ss >> target;
    return target;
}

template<typename T>
std::wstring formatSimple(const std::wstring& input, T arg0)
{
    std::wostringstream ss;
    std::size_t lastCloseBrace = std::wstring::npos;
    std::size_t openBrace = std::wstring::npos;
    while ((openBrace = input.find(L'{', openBrace + 1)) != std::wstring::npos) {
        if (openBrace + 1 < input.length()) {
            if (input[openBrace + 1] == L'{') {
                openBrace++;
                continue;
            }

            std::size_t closeBrace = input.find(L'}', openBrace + 1);
            if (closeBrace != std::wstring::npos) {
                ss << input.substr(lastCloseBrace + 1, openBrace - lastCloseBrace - 1);
                lastCloseBrace = closeBrace;

                std::wstring index = trim(input.substr(openBrace + 1, closeBrace - openBrace - 1));
                if (index == L"0")
                    ss << arg0;
                else
                    throw std::runtime_error("Only simple positional format specifiers are handled by the 'formatSimple' helper method.");
            }
        }
    }

    if (lastCloseBrace + 1 < input.length())
        ss << input.substr(lastCloseBrace + 1);

    return ss.str();
}

template<typename T1, typename T2>
std::wstring formatSimple(const std::wstring& input, T1 arg0, T2 arg1)
{
    std::wostringstream ss;
    std::size_t lastCloseBrace = std::wstring::npos;
    std::size_t openBrace = std::wstring::npos;
    while ((openBrace = input.find(L'{', openBrace + 1)) != std::wstring::npos) {
        if (openBrace + 1 < input.length()) {
            if (input[openBrace + 1] == L'{') {
                openBrace++;
                continue;
            }

            std::size_t closeBrace = input.find(L'}', openBrace + 1);
            if (closeBrace != std::wstring::npos) {
                ss << input.substr(lastCloseBrace + 1, openBrace - lastCloseBrace - 1);
                lastCloseBrace = closeBrace;

                std::wstring index = trim(input.substr(openBrace + 1, closeBrace - openBrace - 1));
                if (index == L"0")
                    ss << arg0;
                else if (index == L"1")
                    ss << arg1;
                else
                    throw std::runtime_error("Only simple positional format specifiers are handled by the 'formatSimple' helper method.");
            }
        }
    }

    if (lastCloseBrace + 1 < input.length())
        ss << input.substr(lastCloseBrace + 1);

    return ss.str();
}

template<typename T1, typename T2, typename T3>
std::wstring formatSimple(const std::wstring& input, T1 arg0, T2 arg1, T3 arg2)
{
    std::wostringstream ss;
    std::size_t lastCloseBrace = std::wstring::npos;
    std::size_t openBrace = std::wstring::npos;
    while ((openBrace = input.find(L'{', openBrace + 1)) != std::wstring::npos) {
        if (openBrace + 1 < input.length()) {
            if (input[openBrace + 1] == L'{') {
                openBrace++;
                continue;
            }

            std::size_t closeBrace = input.find(L'}', openBrace + 1);
            if (closeBrace != std::wstring::npos) {
                ss << input.substr(lastCloseBrace + 1, openBrace - lastCloseBrace - 1);
                lastCloseBrace = closeBrace;

                std::wstring index = trim(input.substr(openBrace + 1, closeBrace - openBrace - 1));
                if (index == L"0")
                    ss << arg0;
                else if (index == L"1")
                    ss << arg1;
                else if (index == L"2")
                    ss << arg2;
                else
                    throw std::runtime_error("Only simple positional format specifiers are handled by the 'formatSimple' helper method.");
            }
        }
    }

    if (lastCloseBrace + 1 < input.length())
        ss << input.substr(lastCloseBrace + 1);

    return ss.str();
}

#endif // STRINGHELPERS_HPP
