#include "docsandtoolsmanagerui.hpp"

DocsAndToolsManagerUI::DocsAndToolsManagerUI(ultralight::RefPtr<ultralight::Window> win,
                                             ultralight::RefPtr<ultralight::App> app)
    : m_readerWriter("information.json")
{
    m_window = win;
    m_app = app;

    m_sidebar_panel = ultralight::Overlay::Create(*m_window.get(), 1, 1, 0, 0);
    m_main_content = ultralight::Overlay::Create(*m_window.get(), 1, 1, 0, 0);

    OnResize(m_window->width(), m_window->height());

    m_sidebar_panel->view()->LoadURL("file:///sidebar_panel.html");
    m_main_content->view()->LoadURL("file:///about_content.html");

    m_window->set_listener(this);
    m_sidebar_panel->view()->set_load_listener(this);
    m_main_content->view()->set_load_listener(this);

    m_sidebar_panel->view()->set_view_listener(this);
    m_main_content->view()->set_view_listener(this);
}

void DocsAndToolsManagerUI::OnResize(std::uint32_t width, std::uint32_t height) {
    std::uint32_t left_pane_width_px = m_window->DeviceToPixels(LEFT_PANE_WIDTH);
    m_sidebar_panel->Resize(left_pane_width_px, height);

    int right_pane_width = (int)width - left_pane_width_px;

    right_pane_width = right_pane_width > 1 ? right_pane_width: 1;

    m_main_content->Resize((uint32_t)right_pane_width, height);

    m_sidebar_panel->MoveTo(0, 0);
    m_main_content->MoveTo(left_pane_width_px, 0);
}

ultralight::JSValue DocsAndToolsManagerUI::ChangeContent(const ultralight::JSObject& thisObject, const ultralight::JSArgs& args) {

    String html_string = String(args[1].ToString());

    std::string buttonText = std::string(html_string.utf8().data());

    if(buttonText == "add") {
        m_main_content->view()->LoadURL("file:///add_content.html");
    } else if(buttonText == "about") {
        m_main_content->view()->LoadURL("file:///about_content.html");
    } else if(buttonText == "trash") {
        m_main_content->view()->LoadURL("file:///recycle_bin_content.html");
    }

    return ultralight::JSValue();
}


ultralight::JSValue DocsAndToolsManagerUI::addEntry(const ultralight::JSObject& thisObject, const ultralight::JSArgs& args) {
    Json::Value value;
    value["categoryName"] = std::string(String(args[0].ToString()).utf8().data());
    value["title"] = std::string(String(args[1].ToString()).utf8().data());
    value["url"] = std::string(String(args[2].ToString()).utf8().data());

    m_readerWriter.append(value);
    m_readerWriter.save();

    return ultralight::JSValue();
}


void DocsAndToolsManagerUI::OnDOMReady(ultralight::View* caller,
                                       uint64_t frame_id,
                                       bool is_main_frame,
                                       const ultralight::String& url) {
    ultralight::Ref<ultralight::JSContext> context = caller->LockJSContext();
    ultralight::SetJSContext(context.get());

    ultralight::JSObject global = ultralight::JSGlobalObject();
    global["ChangeContent"] = BindJSCallbackWithRetval(&DocsAndToolsManagerUI::ChangeContent);
    global["addEntry"] = BindJSCallbackWithRetval(&DocsAndToolsManagerUI::addEntry);
}
