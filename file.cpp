#include "file.hpp"
#include <fstream>
#include <vector>
#include <filesystem>

File::File(const std::wstring& path)
    : m_path(path)
{
    _computeSize();
}

void File::_computeSize()
{
    if (std::filesystem::exists(m_path) &&
        std::filesystem::is_regular_file(m_path)) {
        auto err = std::error_code{};
        auto filesize = std::filesystem::file_size(m_path, err);
        if (filesize != static_cast<uintmax_t>(-1))
            m_size = filesize;
        else
            m_size = static_cast<uintmax_t>(-1);
    }
}

std::string File::readAllText()
{
    std::ifstream file(m_path);
    std::string content;
    std::string ret;

    while(file >> content) {
        ret += content;
    }

    return ret;
}

std::vector<char> File::readAllBytes()
{
    std::ifstream ifs(m_path, std::ios::binary | std::ios::ate);
    std::ifstream::pos_type pos = ifs.tellg();

    std::vector<char> result(pos);

    ifs.seekg(0, std::ios::beg);
    ifs.read(&result[0], pos);

    return result;
}

void File::writeAllText(const std::string& text)
{
    std::fstream fs;
    fs.open(m_path, std::fstream::out);
    fs << text;
    fs.close();
}
