#ifndef DOCSANDTOOLSMANAGERUI_HPP
#define DOCSANDTOOLSMANAGERUI_HPP

#include <AppCore/App.h>
#include <AppCore/Window.h>
#include <AppCore/Overlay.h>
#include <AppCore/JSHelpers.h>
#include <iostream>
#include <regex>
#include "jsonreaderwriter.hpp"

using namespace ultralight;

#define WINDOW_WIDTH    900
#define WINDOW_HEIGHT   600
#define LEFT_PANE_WIDTH 270

class DocsAndToolsManagerUI :
              public ultralight::WindowListener,
              public ultralight::ViewListener,
              public ultralight::LoadListener {
    ultralight::RefPtr<ultralight::App> m_app;
    ultralight::RefPtr<ultralight::Window> m_window;
    ultralight::RefPtr<ultralight::Overlay> m_sidebar_panel;
    ultralight::RefPtr<ultralight::Overlay> m_main_content;
    JsonReaderWriter m_readerWriter;

public:
    DocsAndToolsManagerUI(ultralight::RefPtr<ultralight::Window> win,
                          ultralight::RefPtr<ultralight::App> app);

    virtual ~DocsAndToolsManagerUI() {
    }

    virtual void OnClose() override {  }

    virtual void OnResize(std::uint32_t width, std::uint32_t height) override;

    virtual void OnChangeCursor(ultralight::View* caller, ultralight::Cursor cursor) {
        m_window->SetCursor(cursor);
    }

    void Run() {
        m_app->Run();
    }

    ultralight::JSValue ChangeContent(const ultralight::JSObject& thisObject, const ultralight::JSArgs& args);
    ultralight::JSValue addEntry(const ultralight::JSObject& thisObject, const ultralight::JSArgs& args);

    virtual void OnDOMReady(ultralight::View* caller,
                             uint64_t frame_id,
                             bool is_main_frame,
                             const ultralight::String& url) override;
};

#endif // DOCSANDTOOLSMANAGERUI_HPP
