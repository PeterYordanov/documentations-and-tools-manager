#ifndef JSONITEMMODEL_HPP
#define JSONITEMMODEL_HPP

#include <string>
#include <json/json.h>

class JsonItemModel
{
public:
    JsonItemModel(const std::string& title,
                  const std::string& url)
    {
        m_data["title"] = title;
        m_data["url"] = url;
    }

    Json::Value getData() {
        return m_data;
    }

private:
    Json::Value m_data;
};

#endif // JSONITEMMODEL_HPP
